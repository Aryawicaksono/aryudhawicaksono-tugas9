import React from "react";
import { SafeAreaView, StatusBar, } from "react-native";
import Routing from "./src/simpleNetworking/Routing";

const App:() => Node =()=>{
  return(
    <SafeAreaView style={{flex:1}}>
      <StatusBar barStyle={'dark-content'}/>
      <Routing/>
    </SafeAreaView>
  )
}
export default App