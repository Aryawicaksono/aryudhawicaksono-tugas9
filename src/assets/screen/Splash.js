import {useEffect} from 'react';
import SplashAtom from '../atom/splashAtom';

const Splash = ({navigation}) => {
  //   useEffect(() => {
  //     setTimeout(() => {
  //       navigation.replace('AuthNavigation');
  //     }, 2000);
  //   });
  return <SplashAtom />;
};

export default Splash;
