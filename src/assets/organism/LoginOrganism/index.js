import React from 'react';
import {View} from 'react-native';

import LoginMolecule from '../../molecule/authMolecule/LoginMolecule';
import IconMolecule from '../../molecule/authMolecule/IconMolecule';
import GeneralButton from '../../atom/GeneralAtom';

export default function LoginOrganism() {
  return (
    <View>
      <LoginMolecule />
      <View style={{height: 10}} />
      <IconMolecule />
      <View style={{height: 70}} />
      <GeneralButton text="Login" />
    </View>
  );
}
