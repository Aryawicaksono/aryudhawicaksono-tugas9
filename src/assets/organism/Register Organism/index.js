import React from 'react';
import {View} from 'react-native';

import RegisterMolecule from '../../molecule/authMolecule/RegisterMolecule';
import IconMolecule from '../../molecule/authMolecule/IconMolecule';
import {GeneralButton} from '../../atom/GeneralAtom';

export default function RegisterOrganism() {
  return (
    <View>
      <RegisterMolecule />
      <View style={{height: 10}} />
      <IconMolecule />
      <View style={{height: 70}} />
      <GeneralButton text="Login" />
    </View>
  );
}
