import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {authstyle} from '../../../Style';

export default function TextButton({text, onPress}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={authstyle.touchableText}>{text}</Text>
    </TouchableOpacity>
  );
}
