import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {authstyle} from '../../../Style';

export default function IconButton({source}) {
  return (
    <TouchableOpacity>
      <Image style={authstyle.icon} source={source} />
    </TouchableOpacity>
  );
}
