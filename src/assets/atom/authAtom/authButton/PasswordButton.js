import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {authstyle} from '../../../Style';

export default function PasswordButton() {
  return (
    <TouchableOpacity>
      <Text style={authstyle.passwordText}>Forgot Password?</Text>
    </TouchableOpacity>
  );
}
