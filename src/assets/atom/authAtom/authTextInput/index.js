import React from 'react';
import {TextInput, Text, View} from 'react-native';
import {authstyle} from '../../../Style';

export default function AuthTextInput({label, text}) {
  return (
    <View>
      <Text style={authstyle.inputText}>{text}</Text>
      <View style={{height: 11}} />
      <TextInput
        style={authstyle.textInput}
        placeholder={label}
        // onChangeText={onChangeText}
      />
    </View>
  );
}
