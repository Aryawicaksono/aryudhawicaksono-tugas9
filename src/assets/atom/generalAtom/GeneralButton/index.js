import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {authstyle} from '../../../Style';

export default function GeneralButton(text) {
  return (
    <TouchableOpacity style={authstyle.button}>
      <Text style={authstyle.buttontext}>{text}</Text>
    </TouchableOpacity>
  );
}
