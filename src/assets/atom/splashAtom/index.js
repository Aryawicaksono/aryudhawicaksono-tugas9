import SplashLogo from './splashLogo';
import {View} from 'react-native';
import React from 'react';
import {splashStyle} from '../../Style';

export default function SplashAtom() {
  return (
    <View style={splashStyle.Layout}>
      <SplashLogo />
    </View>
  );
}
