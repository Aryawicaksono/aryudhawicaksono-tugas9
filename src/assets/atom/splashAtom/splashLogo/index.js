import {Image} from 'react-native';
import React from 'react';
import {Logo} from '../../Image';
import splashStyle from '../../../Style/splashStyle';

const SplashLogo = () => {
  return <Image source={Logo} style={splashStyle.Logo} />;
};
export default SplashLogo;
