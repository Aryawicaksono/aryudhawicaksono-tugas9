import color from '../color';
import {StyleSheet} from 'react-native';

const splashStyle = StyleSheet.create({
  Layout: {
    flex: 1,
    backgroundColor: color.splashColor.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Logo: {
    height: 238,
    width: 230,
  },
});
export default splashStyle;
