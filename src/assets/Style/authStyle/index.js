import {StyleSheet} from 'react-native';
import color from '../color';

const authstyle = StyleSheet.create({
  layout: {
    width: '100%',
    backgroundColor: color.authColor.white,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: 31,
    paddingHorizontal: 20,
  },
  button: {
    width: 335,
    height: 55,
    color: color.authColor.red,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    height: 45,
    width: 335,
    backgroundColor: color.authColor.lightGray,
    borderRadius: 5,
  },
  imageBackGround: {
    width: '100%',
    resizeMode: 'contain',
  },
  icon: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  headerText: {
    fontFamily: 'monserrat',
    fontSize: 24,
    fontWeight: '700',
    color: color.authColor.dark,
  },
  inputText: {
    fontSize: 12,
    fontWeight: '600',
    color: color.authColor.red,
  },
  passwordText: {
    fontSize: 10,
    fontWeight: '300',
    color: color.authColor.gray,
  },
  footertext: {
    fontSize: 12,
    fontWeight: '500',
    color: color.authColor.gray,
  },
  buttontext: {
    fontSize: 16,
    fontWeight: '700',
    color: color.authColor.white,
  },
  touchableText: {
    fontSize: 12,
    fontWeight: '500',
    color: color.authColor.red,
  },
});
export default authstyle;
