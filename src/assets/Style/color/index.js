const color = {
  splashColor: {
    white: '#FFFFFF',
  },

  authColor: {
    white: '#FFFFFF',
    dark: '#0A0827',
    red: '#BB2427',
    gray: '#717171',
    lightGray: '#F6F8FF',
  },
};

export default color;
