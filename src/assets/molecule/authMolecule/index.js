import LoginMolecule from './LoginMolecule';
import RegisterMolecule from './RegisterMolecule';
import IconMolecule from './IconMolecule';
import FooterMolecule from './footerMolecule';
import HeaderMolecule from './HeaderMolecule';

export default {
  LoginMolecule,
  RegisterMolecule,
  IconMolecule,
  FooterMolecule,
  HeaderMolecule,
};
