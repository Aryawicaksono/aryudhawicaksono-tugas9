import React from 'react';
import {View} from 'react-native';
import {AuthTextInput} from '../../../atom/textInput';

export default function RegisterMolecule() {
  return (
    <View>
      <AuthTextInput text="Nama" label="Enter Your Name" />
      <View style={{height: 10}} />
      <AuthTextInput text="Email" label="yourmail@mail.com" />
      <View style={{height: 10}} />
      <AuthTextInput text="Password" label="password" />
      <View style={{height: 10}} />
      <AuthTextInput text="Confirm Password" label="password" />
    </View>
  );
}
