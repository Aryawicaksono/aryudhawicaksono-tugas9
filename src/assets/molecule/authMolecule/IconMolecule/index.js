import React from 'react';
import {View} from 'react-native';
import IconButton from '../../../atom/authAtom/authButton/IconButton';
import PasswordButton from '../../../atom/authAtom/authButton/PasswordButton';
import {Gmail, facbook, Twitter} from '../../../atom/Icon/AuthIcon';

export default function IconMolecule() {
  return (
    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <View style={{flexDirection: 'row'}}>
        <IconButton source={Gmail} />
        <View style={{width: 10}} />
        <IconButton source={facbook} />
        <View style={{width: 10}} />
        <IconButton source={Twitter} />
      </View>
      <PasswordButton />
    </View>
  );
}
