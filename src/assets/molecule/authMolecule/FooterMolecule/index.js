import {Text, View} from 'react-native';
import React from 'react';
import TextButton from '../../../atom/authAtom/authButton/TextButton';
import {authstyle} from '../../../Style';

export default function FooterMolecule({text, navigation, pages}) {
  return (
    <View style={{flexDirection: 'row'}}>
      <Text style={authstyle.footertext}>{text}</Text>
      <View style={{width: 5}} />
      <TextButton onPress={navigation.navigate({pages})} />
    </View>
  );
}
