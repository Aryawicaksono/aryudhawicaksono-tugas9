import React from 'react';
import {Text} from 'react-native';
import {authstyle} from '../../../Style';

export default function HeaderMolecule(text) {
  return (
    <Text style={authstyle.headerText}>
      Welcome,{'\n'}
      {text}
    </Text>
  );
}
