import React from 'react';
import {View} from 'react-native';
import {AuthTextInput} from '../../../atom/textInput';

export default function LoginMolecule() {
  return (
    <View>
      <AuthTextInput text="Email" label="yourmail@mail.com" />
      <View style={{height: 10}} />
      <AuthTextInput text="Password" label="password" />
    </View>
  );
}
