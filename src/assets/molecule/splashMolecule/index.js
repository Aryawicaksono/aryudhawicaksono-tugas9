import {View} from 'react-native';
import React from 'react';
import {splashStyle} from '../../Style';
import SplashLogo from '../../atom/splashAtom/splashLogo';

export default function SplashMolecule() {
  return (
    <View style={splashStyle.Layout}>
      <SplashLogo />
    </View>
  );
}
