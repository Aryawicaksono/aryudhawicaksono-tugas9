import axios from 'axios';
import {BASE_URL, TOKEN} from './url';
import {Alert} from 'react-native';

const headers = {
  headers: {
    'Content-Type': 'application/json',
    Authorization: TOKEN,
  },
};

const getDataMobilHelper = async (setDataMobil, setIsLoading) => {
  console.log('Getting Data Mobil');
  axios
    .get(`${BASE_URL}mobil`, headers)
    .then(response => {
      console.log('res get mobil', response);

      if (response.status === 200) {
        const result = response.data.items;
        console.log('response JSON: ', result);
        setDataMobil(result);
        setIsLoading(false);
      } else if (response.status === 401 || response.status === 402) {
        return false;
      } else if (response.status === 500) {
        return false;
      }
    })
    .catch(error => {
      console.log('error get mobil', error);
    });
};

const postDataHelper = async body => {
  axios
    .post(`${BASE_URL}mobil`, body, headers) // wajib body dlu baru header
    .then(response => {
      console.log('response success: ', response);
      if (response.status === 200 || response.status === 201) {
        Alert.alert('Data Mobil berhasil ditambahkan');
      }
    })
    .catch(error => console.log('error add data: ', error));
};

const editDataHelper = async body => {
  axios
    .put(`${BASE_URL}mobil`, body, headers)
    .then(response => {
      if (response.status === 200 || response.status === 201) {
        Alert.alert('Data Mobil berhasil dirubah');
      }
    })
    .catch(error => console.log('error update data: ', error));
};

const deleteDataHelper = async body => {
  axios
    .delete(`${BASE_URL}mobil`, {data: body, ...headers})
    .then(response => {
      if (response.status === 200 || response.status === 201) {
        Alert.alert('Data Mobil berhasil dihapus');
      }
    })
    .catch(error => console.log('error delete data: ', error));
};

export {getDataMobilHelper, postDataHelper, editDataHelper, deleteDataHelper};
