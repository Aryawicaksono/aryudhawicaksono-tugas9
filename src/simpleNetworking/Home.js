// import {Text, View, Image, TouchableOpacity, Dimensions} from 'react-native';
// import React, {useEffect, useState} from 'react';
// import Icon from 'react-native-vector-icons/AntDesign';
// import {TOKEN, BASE_URL} from './url';
// import {useIsFocused} from '@react-navigation/native';
// import Axios from 'axios';
// import Carousel from 'react-native-snap-carousel';
// import {getDataMobilHelper} from './helpers';

// export default function Home({navigation}) {
//   const isFocused = useIsFocused();

//   const dimension = Dimensions.get('window');
//   console.log(dimension);
//   const carouselWidth = dimension.width;
//   const carouselItemWidth = carouselWidth * 0.9;
//   useEffect(() => {
//     getDataMobil();
//   }, [isFocused]);

//   const [dataMobil, setDataMobil] = useState('');

//   const getDataMobil = async () => {
//     getDataMobilHelper();
//   };

//   const convertCurrency = (nominal = 0, currency) => {
//     let rupiah = '';
//     const nominalref = nominal.toString().split('').reverse().join('');
//     for (let i = 0; i < nominalref.length; i++) {
//       if (i % 3 === 0) {
//         rupiah += nominalref.substr(i, 3) + '.';
//       }
//     }

//     if (currency) {
//       return (
//         currency +
//         rupiah
//           .split('', rupiah.length - 1)
//           .reverse()
//           .join('')
//       );
//     } else {
//       return rupiah
//         .split('', rupiah.length - 1)
//         .reverse()
//         .join('');
//     }
//   };

//   return (
//     <View style={{flex: 1, backgroundColor: '#fff'}}>
//       <Text
//         style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
//         Home screen
//       </Text>
//       <Carousel
//         data={dataMobil}
//         sliderWidth={carouselWidth}
//         itemWidth={carouselItemWidth}
//         keyExtractor={(item, index) => index.toString()}
//         renderItem={({item, index}) => (
//           <View>
//             <TouchableOpacity
//               onPress={() => navigation.navigate('AddData', item)}
//               activeOpacity={0.8}
//               style={{
//                 width: '90%',
//                 alignSelf: 'center',
//                 marginTop: 15,
//                 borderColor: '#dedede',
//                 borderWidth: 1,
//                 borderRadius: 6,
//                 padding: 12,
//               }}>
//               <View
//                 style={{
//                   justifyContent: 'center',
//                   alignItems: 'center',
//                 }}>
//                 <Image
//                   style={{
//                     width: '100%',
//                     height: 200,
//                     resizeMode: 'contain',
//                   }}
//                   source={{uri: item.unitImage}}
//                 />
//               </View>
//               <View style={{height: 20}} />
//               <View
//                 style={{
//                   paddingHorizontal: 10,
//                   alignSelf: 'center',
//                 }}>
//                 <View
//                   style={{
//                     width: '100%',
//                     flexDirection: 'row',
//                     alignItems: 'center',
//                   }}>
//                   <Text
//                     style={{fontWeight: '700', fontSize: 20, color: '#000'}}>
//                     Nama Mobil :
//                   </Text>
//                   <Text style={{fontSize: 20, color: '#000'}}>
//                     {' '}
//                     {item.title}
//                   </Text>
//                 </View>
//                 <View style={{height: 10}} />
//                 <View
//                   style={{
//                     width: '100%',
//                     flexDirection: 'row',
//                     alignItems: 'center',
//                   }}>
//                   <Text
//                     style={{fontWeight: '700', fontSize: 20, color: '#000'}}>
//                     Total KM :
//                   </Text>
//                   <Text style={{fontSize: 20, color: '#000'}}>
//                     {' '}
//                     {item.totalKM}
//                   </Text>
//                 </View>
//                 <View style={{height: 10}} />
//                 <View
//                   style={{
//                     width: '100%',
//                     flexDirection: 'row',
//                     alignItems: 'center',
//                   }}>
//                   <Text
//                     style={{fontWeight: '700', fontSize: 20, color: '#000'}}>
//                     Harga Mobil :
//                   </Text>
//                   <Text style={{fontSize: 20, color: '#000'}}>
//                     {' '}
//                     {convertCurrency(item.harga, 'Rp. ')}
//                   </Text>
//                 </View>
//               </View>
//             </TouchableOpacity>
//           </View>
//         )}
//       />
//       <TouchableOpacity
//         style={{
//           position: 'absolute',
//           bottom: 30,
//           right: 10,
//           width: 40,
//           height: 40,
//           borderRadius: 20,
//           backgroundColor: 'red',
//           justifyContent: 'center',
//           alignItems: 'center',
//         }}
//         onPress={() => navigation.navigate('AddData')}>
//         <Icon name="plus" size={20} color="#fff" />
//       </TouchableOpacity>
//     </View>
//   );
// }

import {Text, View, Dimensions, Image, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import {TOKEN, BASE_URL} from './url';
import {useIsFocused} from '@react-navigation/native';
import Axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import {getDataMobilHelper} from './helpers';

export default function Home({navigation}) {
  const isFocused = useIsFocused();
  const dimension = Dimensions.get('window');
  const carouselWidth = dimension.width;
  const carouselItemWidth = carouselWidth * 0.9;

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const [dataMobil, setDataMobil] = useState([]);

  // const getDataMobil = async () => {
  //   Axios.get(`${BASE_URL}mobil`, {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: TOKEN,
  //     },
  //   })
  //     .then(response => {
  //       console.log('res get mobil', response);
  //       if (response.status === 200) {
  //         setDataMobil(response.data.items);
  //       } else {
  //         if (response.status === 401 || response.status === 401) {
  //           return false;
  //         }
  //       }
  //     })
  //     .catch(error => {
  //       console.log('error get mobil', error);
  //     });
  // };
  const getDataMobil = async () => {
    try {
      const data = await getDataMobilHelper();
      if (data) {
        setDataMobil(data);
      }
    } catch (error) {
      console.log('error get mobil', error);
    }
  };

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <Carousel
        sliderWidth={carouselWidth}
        itemWidth={carouselItemWidth}
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <View>
            <TouchableOpacity
              onPress={() => navigation.navigate('AddData', item)}
              activeOpacity={0.8}
              style={{
                width: '90%',
                alignSelf: 'center',
                marginTop: 15,
                borderColor: '#dedede',
                borderWidth: 1,
                borderRadius: 6,
                padding: 12,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  style={{
                    width: '100%',
                    height: 200,
                    resizeMode: 'contain',
                  }}
                  source={{uri: item.unitImage}}
                />
              </View>
              <View style={{height: 20}} />
              <View
                style={{
                  paddingHorizontal: 10,
                  alignSelf: 'center',
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '700', fontSize: 20, color: '#000'}}>
                    Nama Mobil :
                  </Text>
                  <Text style={{fontSize: 20, color: '#000'}}>
                    {' '}
                    {item.title}
                  </Text>
                </View>
                <View style={{height: 10}} />
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '700', fontSize: 20, color: '#000'}}>
                    Total KM :
                  </Text>
                  <Text style={{fontSize: 20, color: '#000'}}>
                    {' '}
                    {item.totalKM}
                  </Text>
                </View>
                <View style={{height: 10}} />
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{fontWeight: '700', fontSize: 20, color: '#000'}}>
                    Harga Mobil :
                  </Text>
                  <Text style={{fontSize: 20, color: '#000'}}>
                    {' '}
                    {convertCurrency(item.harga, 'Rp. ')}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
      />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
    </View>
  );
}
