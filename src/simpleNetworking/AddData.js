import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './url';
import Axios from 'axios';

import {postDataHelper, putDataHelper, deleteDataHelper} from './helpers';

const AddData = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  var dataMobil = route.params;

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  const postData = async () => {
    try {
      const response = await postDataHelper(namaMobil, totalKM, hargaMobil);
      if (response.status === 200 || response.status === 201) {
        alert('Data mobil berhasil ditambahkan');
        navigation.goBack();
      }
    } catch (error) {
      console.log('error add', error);
    }
  };

  const editData = async () => {
    try {
      const response = await putDataHelper(
        dataMobil._uuid,
        namaMobil,
        totalKM,
        hargaMobil,
      );
      if (response.status === 200 || response.status === 201) {
        alert('Data mobil berhasil dimasukkan');
        navigation.goBack();
      }
    } catch (error) {
      console.log('error edit', error);
    }
  };
  const deleteData = async () => {
    try {
      const response = await deleteDataHelper(dataMobil._uuid);
      if (response.status === 200 || response.status === 201) {
        alert('Data mobil berhasil dihapus');
        navigation.goBack();
      }
    } catch (error) {
      console.log('error delete', error);
    }
  };
  const Syarat = value => {
    if (namaMobil === '') {
      return alert('Nama mobil tidak boleh kosong');
    }
    if (hargaMobil <= 100000000) {
      return alert('Harga mobil dibawah 100juta');
    }
    if (totalKM === '') {
      return alert('Kilometer tidak boleh kosong');
    }
    switch (value) {
      case 'post':
        postData();
        break;
      case 'edit':
        editData();
        break;
      default:
        break;
    }

    // namaMobil === ''
    //   ? alert('Nama mobil tidak boleh kosong')
    //   : hargaMobil <= 100000000
    //   ? alert('Harga mobil dibawah 100juta')
    //   : totalKM === ''
    //   ? alert('Total kilometer tidak boleh kosong')
    //   : deleteData();
  };
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrowleft" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          Tambah Data
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            value={namaMobil}
            onChangeText={text => setNamaMobil(text)}
            placeholder="Masukkan Nama Mobil"
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            value={totalKM}
            onChangeText={text => setTotalKM(text)}
            placeholder="contoh: 100 KM"
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            value={hargaMobil}
            onChangeText={text => setHargaMobil(text)}
            placeholder="Masukkan Harga Mobil"
            style={styles.txtInput}
            keyboardType="number-pad"
          />
        </View>
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => (dataMobil ? Syarat('edit') : Syarat('post'))}>
          <Text style={{color: '#fff', fontWeight: '600'}}>
            {dataMobil ? 'Ubah data' : 'Tambah data'}
          </Text>
        </TouchableOpacity>
        {dataMobil ? (
          <TouchableOpacity
            onPress={() => deleteData()}
            style={[styles.btnAdd, {backgroundColor: 'red'}]}>
            <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
