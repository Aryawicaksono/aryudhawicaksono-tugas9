// apiHelper.js
import Axios from 'axios';
import {BASE_URL, TOKEN} from './url';

// Fungsi untuk melakukan permintaan GET dataMobil
export const getDataMobilHelper = async () => {
  try {
    const response = await Axios.get(`${BASE_URL}mobil`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    });

    if (response.status === 200) {
      return response.data.items;
    }

    return null;
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }
};

// Fungsi untuk melakukan permintaan POST
export const postDataHelper = async (namaMobil, totalKM, hargaMobil) => {
  const body = [
    {
      title: namaMobil,
      harga: hargaMobil,
      totalKM: totalKM,
      unitImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
    },
  ];

  const options = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: TOKEN,
    },
  };

  try {
    const response = await Axios.post(`${BASE_URL}mobil`, body, options);
    return response;
  } catch (error) {
    throw error;
  }
};

// Fungsi untuk melakukan permintaan PUT
export const putDataHelper = async (_uuid, namaMobil, totalKM, hargaMobil) => {
  const body = [
    {
      _uuid: _uuid,
      title: namaMobil,
      harga: hargaMobil,
      totalKM: totalKM,
      unitImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
    },
  ];

  const options = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: TOKEN,
    },
  };

  try {
    const response = await Axios.put(`${BASE_URL}mobil`, body, options);
    return response;
  } catch (error) {
    throw error;
  }
};

// Fungsi untuk melakukan permintaan DELETE
export const deleteDataHelper = async _uuid => {
  const options = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: TOKEN,
    },
  };

  try {
    const response = await Axios.delete(`${BASE_URL}mobil/${_uuid}`, options);
    return response;
  } catch (error) {
    throw error;
  }
};
